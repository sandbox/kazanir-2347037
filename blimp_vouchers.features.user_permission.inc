<?php
/**
 * @file
 * blimp_vouchers.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function blimp_vouchers_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer commerce_coupon entities'.
  $permissions['administer commerce_coupon entities'] = array(
    'name' => 'administer commerce_coupon entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'commerce_coupon',
  );

  // Exported permission: 'create commerce_coupon entities of bundle blimp_voucher'.
  $permissions['create commerce_coupon entities of bundle blimp_voucher'] = array(
    'name' => 'create commerce_coupon entities of bundle blimp_voucher',
    'roles' => array(
      'administrator' => 'administrator',
      'commerce guy' => 'commerce guy',
      'sales team' => 'sales team',
      'webmaster' => 'webmaster',
    ),
    'module' => 'commerce_coupon',
  );

  // Exported permission: 'edit any commerce_coupon entity of bundle blimp_voucher'.
  $permissions['edit any commerce_coupon entity of bundle blimp_voucher'] = array(
    'name' => 'edit any commerce_coupon entity of bundle blimp_voucher',
    'roles' => array(
      'administrator' => 'administrator',
      'webmaster' => 'webmaster',
    ),
    'module' => 'commerce_coupon',
  );

  // Exported permission: 'edit own commerce_coupon entities of bundle blimp_voucher'.
  $permissions['edit own commerce_coupon entities of bundle blimp_voucher'] = array(
    'name' => 'edit own commerce_coupon entities of bundle blimp_voucher',
    'roles' => array(
      'administrator' => 'administrator',
      'commerce guy' => 'commerce guy',
      'sales team' => 'sales team',
      'webmaster' => 'webmaster',
    ),
    'module' => 'commerce_coupon',
  );

  // Exported permission: 'redeem any coupon'.
  $permissions['redeem any coupon'] = array(
    'name' => 'redeem any coupon',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'commerce_coupon',
  );

  // Exported permission: 'redeem coupons of type blimp_voucher'.
  $permissions['redeem coupons of type blimp_voucher'] = array(
    'name' => 'redeem coupons of type blimp_voucher',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'commerce_coupon',
  );

  // Exported permission: 'redeem non user specific coupons of type blimp_voucher'.
  $permissions['redeem non user specific coupons of type blimp_voucher'] = array(
    'name' => 'redeem non user specific coupons of type blimp_voucher',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'commerce_coupon_user',
  );

  // Exported permission: 'redeem received coupons of type blimp_voucher'.
  $permissions['redeem received coupons of type blimp_voucher'] = array(
    'name' => 'redeem received coupons of type blimp_voucher',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'commerce_coupon_user',
  );

  // Exported permission: 'view any commerce_coupon entity of bundle blimp_voucher'.
  $permissions['view any commerce_coupon entity of bundle blimp_voucher'] = array(
    'name' => 'view any commerce_coupon entity of bundle blimp_voucher',
    'roles' => array(
      'administrator' => 'administrator',
      'webmaster' => 'webmaster',
    ),
    'module' => 'commerce_coupon',
  );

  // Exported permission: 'view non user specific coupons of type blimp_voucher'.
  $permissions['view non user specific coupons of type blimp_voucher'] = array(
    'name' => 'view non user specific coupons of type blimp_voucher',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'commerce_coupon_user',
  );

  // Exported permission: 'view own commerce_coupon entities of bundle blimp_voucher'.
  $permissions['view own commerce_coupon entities of bundle blimp_voucher'] = array(
    'name' => 'view own commerce_coupon entities of bundle blimp_voucher',
    'roles' => array(
      'administrator' => 'administrator',
      'commerce guy' => 'commerce guy',
      'sales team' => 'sales team',
      'webmaster' => 'webmaster',
    ),
    'module' => 'commerce_coupon',
  );

  // Exported permission: 'view received coupons of type blimp_voucher'.
  $permissions['view received coupons of type blimp_voucher'] = array(
    'name' => 'view received coupons of type blimp_voucher',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'commerce_coupon_user',
  );

  return $permissions;
}
