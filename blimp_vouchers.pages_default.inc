<?php

/**
 * @file
 * blimp_vouchers.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function blimp_vouchers_default_page_manager_pages() {

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'account_voucher_credit';
  $page->task = 'page';
  $page->admin_title = 'Account Voucher Credit';
  $page->admin_description = '';
  $page->path = 'user/%user/vouchers/!coupon';
  $page->access = array();
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Vouchers',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'user' => array(
      'id' => 1,
      'identifier' => 'User: ID',
      'name' => 'entity_id:user',
      'settings' => array(),
    ),
    'coupon' => array(
      'id' => 1,
      'identifier' => 'Commerce Coupon: ID',
      'name' => 'entity_id:commerce_coupon',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_account_voucher_credit_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'account_voucher_credit';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Credit Summary',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '0',
          ),
          'context' => 'argument_entity_id:commerce_coupon_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Account Credit Summary';
  $display->uuid = '02f2b466-7854-4023-8d53-d254b0791a0f';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-757f24cb-2f85-430d-8ace-bbb72174c73d';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'blimp_vouchers_account_credit-summary_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'arguments' => array(
        'uid' => '%user:uid',
        'uid_1' => '%user:uid',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '757f24cb-2f85-430d-8ace-bbb72174c73d';
    $display->content['new-757f24cb-2f85-430d-8ace-bbb72174c73d'] = $pane;
    $display->panels['middle'][0] = 'new-757f24cb-2f85-430d-8ace-bbb72174c73d';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-757f24cb-2f85-430d-8ace-bbb72174c73d';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_account_voucher_credit_panel_context_2';
  $handler->task = 'page';
  $handler->subtask = 'account_voucher_credit';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Coupon Details',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'argument_entity_id:commerce_coupon_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '2748eac7-9d16-452a-b337-9c15811e3cf9';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-e1f322c3-39f3-42f1-b5e5-a4626f6bb6e3';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'blimp_vouchers_account_credit-per_coupon_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'arguments' => array(
        'uid' => '%user:uid',
        'uid_1' => '%user:uid',
        'coupon_id' => '%coupon:coupon-id',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e1f322c3-39f3-42f1-b5e5-a4626f6bb6e3';
    $display->content['new-e1f322c3-39f3-42f1-b5e5-a4626f6bb6e3'] = $pane;
    $display->panels['middle'][0] = 'new-e1f322c3-39f3-42f1-b5e5-a4626f6bb6e3';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-e1f322c3-39f3-42f1-b5e5-a4626f6bb6e3';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;

  $pages['account_voucher_credit'] = $page;
  
  return $pages;
}