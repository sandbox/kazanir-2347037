<?php

function blimp_vouchers_schema() {
  $schema = array();
  
  $schema['blimp_vouchers_batch'] = array(
    'description' => 'The base table for voucher batches.',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for the batch.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'An identifier for the user who created this batch',
        'type' => 'int',      
        'not null' => TRUE,
        'default' => 0,          
      ),
      'type' => array(
        'description' => 'The type of this batch.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'label' => array(
        'description' => 'The label of this batch.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'prefix' => array(
        'description' => 'The prefix used for voucher codes in this batch.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => NULL,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the batch was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the batch was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),        
      'data' => array(
        'type' => 'blob',
        'size' => 'big',
        'not null' => FALSE,
        'serialize' => TRUE,
        'description' => 'Everything else, serialized.',
      ),        
    ),
    'indexes' => array(
      'uid' => array('uid'),
    ),
    'unique keys' => array(
      'prefix' => array('prefix'),
    ),
    'primary key' => array('id'),
    'foreign keys' => array(
      'uid' => array(
        'table' => 'users',
        'column' => array('uid' => 'uid')
      ),        
    ),
  );
  
  return $schema;
}

/**
 * Add the batch entity table.
 */
function blimp_vouchers_update_7100(&$sandbox) {
  $schema = blimp_vouchers_schema();
  db_create_table('blimp_vouchers_batch', $schema['blimp_vouchers_batch']);
  
  return "Created batch entity table.";
}

/**
 * Add the prefix column to batch entities.
 */
function blimp_vouchers_update_7101(&$sandbox) {
  $schema = blimp_vouchers_schema();
  db_add_field('blimp_vouchers_batch', 'prefix', $schema['blimp_vouchers_batch']['fields']['prefix']);
  db_add_unique_key('blimp_vouchers_batch', 'prefix', array('prefix'));

  return "Added batch prefix column with unique index.";
}