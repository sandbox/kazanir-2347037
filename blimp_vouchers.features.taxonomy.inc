<?php
/**
 * @file
 * blimp_vouchers.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function blimp_vouchers_taxonomy_default_vocabularies() {
  return array(
    'voucher_tags' => array(
      'name' => 'Voucher Tags',
      'machine_name' => 'voucher_tags',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
