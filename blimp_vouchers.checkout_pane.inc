<?php

/*
 * Checkout pane callback: coupon settings
 */
// function blimp_vouchers_redeem_pane_settings_form($checkout_pane) {
//   $form = array();
// 
//   return $form;
// }

/*
 * Checkout pane callback: coupon checkout form
 */
function blimp_vouchers_redeem_checkout_form($form, &$form_state, $checkout_pane, $order) {
  // Allow to replace pane content with ajax calls.
  $pane_form = array(
    '#prefix' => '<div id="blimp-vouchers-redeem-ajax-wrapper">',
    '#suffix' => '</div>',
  );
  
  if ($order->type != 'platform') {
    $pane_form['#access'] = FALSE;
  }

  // Store the payment methods in the form for validation purposes.
  $pane_form['coupon_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Voucher Code'),
    '#description' => t('Enter your coupon code here.'),
  );

  $pane_form['voucher_redeem'] = array(
    '#type' => 'button',
    '#value' => t('Redeem Voucher'),
    '#name' => 'voucher_redeem',
    '#limit_validation_errors' => array(),
    '#ajax' => array(
      'callback' => 'blimp_vouchers_redeem_ajax_callback',
      'wrapper' => 'blimp-vouchers-redeem-ajax-wrapper',
    ),
  );
  
  $error = '';
  if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#name'] == 'voucher_redeem') {
    $code = $form_state['input']['blimp_vouchers_redeem']['coupon_code'];
    if (!empty($code)) {
      // @todo: Fix this.
      $account = user_load($order->uid);
      $coupon = commerce_coupon_load_by_code($code);
        if (!$coupon) {
          $error = t('We\'re sorry, but this doesn\'t appear to be a valid Marketplace voucher code.');
        }
        elseif (!$account) {
          $error = t('We\'re sorry, but you must be logged in to redeem a Marketplace voucher code.');
        }
        else {
          $check = blimp_vouchers_redeem_coupon_check($coupon, $account, $order->order_id);
          if ($check !== TRUE) {
            $error = $check;
          }
          else {
            $pane_form['coupon_code']['#value'] = '';
            blimp_vouchers_redeem_coupon($coupon, $account);
            drupal_set_message(t('Voucher code %code has been applied to your account and will be credited towards your Platform order balance.', array('%code' => $coupon->code)));
          }
        }
    }
    else {
      $error = t('Please enter a code.');
    } 
  }
  
  if (!empty($error)) {
    drupal_set_message($error, 'error');
  }
  
  // Display any new status messages added by this pane within the pane's area.
  if (drupal_get_messages(NULL, FALSE)) {
    $pane_form['status_messages'] = array(
      '#type' => 'markup',
      '#markup' => theme('status_messages'),
      '#weight' => -1,
    );
  }  
  

  return $pane_form;
}

/*
 * Ajax callback: coupon add button.
 */
function blimp_vouchers_redeem_ajax_callback($form, &$form_state) {
  // Re-render coupon pane
  $coupon_pane = $form['blimp_vouchers_redeem'];
  $commands[] = ajax_command_replace('#blimp-vouchers-redeem-ajax-wrapper', drupal_render($coupon_pane));
  
  // Allow other modules to alter the commands
  drupal_alter('blimp_vouchers_redeem_ajax', $commands, $form, $form_state);

  return array('#type' => 'ajax', '#commands' => $commands);
}

// @todo: Construct a review callback.

// /*
//  * Checkout pane callback: coupon checkout review
//  */
// function commerce_coupon_pane_review($form, $form_state, $checkout_pane, $order) {
//   if (!empty($order->commerce_coupons)) {
//     // Extract the View and display keys from the cart contents pane setting.
//     list($view_id, $display_id) = explode('|', variable_get('commerce_coupon_review_pane_view', 'order_coupon_list|checkout'));
// 
//     return commerce_embed_view($view_id, $display_id, array($order->order_id));
//   }
// }
