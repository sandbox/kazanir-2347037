<?php

class blimp_vouchers_handler_field_condition_settings extends views_handler_field {

  function render($values) {
    $data = unserialize($values->{$this->field_alias});
    if ($data && is_array($data)) {
      return $data[$this->definition['settings key']];
    }
  }

}