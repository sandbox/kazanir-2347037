<?php

class blimp_vouchers_handler_field_coupon_credit_balance extends views_handler_field_custom {

  function render($values) {
    $running_total = &drupal_static('blimp_vouchers_account_credit_balance', 0);
    $account = user_load($this->view->args[0]);
    $currency = blimp_vouchers_determine_user_currency($account);
    $fieldname = 'field_' . _blimp_vouchers_price_fields($currency); 
    $credit_field = $values->{$fieldname}[0];
    
    $used_field = (!empty($values->field_commerce_total[0]) ? $values->field_commerce_total[0] : array('raw' => array('amount' => 0)));
    
    $amount = $credit_field['raw']['amount'] + $used_field['raw']['amount'];
    $running_total += $amount;
    $value = commerce_currency_format($amount, $currency);

    return $value;
  }

}