<?php

function blimp_vouchers_views_data_alter(&$data) {
  $data['commerce_coupon']['blimp_vouchers_account_credit'] = array(
    'group' => t('Commerce Coupon'),
    'title' => t('Coupon account credit'),
    'help' => t('The account credit this coupon makes available to a particular account.'),
    'real field' => 'coupon_id',
    'field' => array(
      'handler' => 'blimp_vouchers_handler_field_coupon_account_credit',
    ),
  );
  $data['commerce_coupon']['blimp_vouchers_account_balance'] = array(
    'group' => t('Commerce Coupon'),
    'title' => t('Coupon account balance'),
    'help' => t('The balance remaining on an account from this coupon.'),
    'real field' => 'coupon_id',
    'field' => array(
      'handler' => 'blimp_vouchers_handler_field_coupon_credit_balance',
    ),
  );
  $data['commerce_coupon']['blimp_vouchers_add_form'] = array(
    'group' => t('Commerce Coupon'),
    'title' => t('Add voucher form'),
    'help' => t('Display a form to add a voucher credit to a user account.'),
    'area' => array(
      'handler' => 'blimp_vouchers_handler_area_add_voucher',
    ),
  );
  $data['commerce_coupon']['blimp_vouchers_account_summary'] = array(
    'group' => t('Commerce Coupon'),
    'title' => t('Account summary'),
    'help' => t('Display an summary of an account\'s Marketplace credits.'),
    'area' => array(
      'handler' => 'blimp_vouchers_handler_area_account_summary',
    ),
  );

}

function blimp_vouchers_field_views_data_alter(&$result, $field, $module) {
  if ($field['field_name'] == 'commerce_coupon_conditions') {
    $result['blimp_vouchers_maximum_usage']['table'] = $result['field_data_commerce_coupon_conditions']['table'];
    $result['blimp_vouchers_maximum_usage']['table']['join']['commerce_coupon']['table'] = 'field_data_commerce_coupon_conditions';
    $result['blimp_vouchers_maximum_usage']['table']['join']['commerce_coupon']['extra'][] = array(
      'field' => 'commerce_coupon_conditions_condition_name',
      'value' => 'blimp_vouchers_maximum_usage',
    );
    $result['blimp_vouchers_maximum_usage']['max_usage'] = array(
      'group' => t('Commerce Coupon'),
      'title' => t('Condition: Max Usage'),
      'help' => t('The usage conditions field on a voucher, if any.'),
      'real field' => 'commerce_coupon_conditions_condition_settings',
      'field' => array(
        'settings key' => 'max_usage',
        'handler' => 'blimp_vouchers_handler_field_condition_settings',
        'real field' => 'commerce_coupon_conditions_condition_settings',
        'field_name' => 'conditions_max_usage',
        'click sortable' => FALSE,
      ),
    );

    $result['commerce_coupon_date_evaluate_date']['table'] = $result['field_data_commerce_coupon_conditions']['table'];
    $result['commerce_coupon_date_evaluate_date']['table']['join']['commerce_coupon']['table'] = 'field_data_commerce_coupon_conditions';
    $result['commerce_coupon_date_evaluate_date']['table']['join']['commerce_coupon']['extra'][] = array(
      'field' => 'commerce_coupon_conditions_condition_name',
      'value' => 'commerce_coupon_date_evaluate_date',
    );
    $result['commerce_coupon_date_evaluate_date']['start_date'] = array(
      'group' => t('Commerce Coupon'),
      'title' => t('Condition: Start Date'),
      'help' => t('The start date condition field on a voucher, if any.'),
      'real field' => 'commerce_coupon_conditions_condition_settings',
      'field' => array(
        'settings key' => 'start',
        'handler' => 'blimp_vouchers_handler_field_condition_settings',
        'real field' => 'commerce_coupon_conditions_condition_settings',
        'field_name' => 'condition_start_date',
        'click sortable' => FALSE,
      ),
    );
    $result['commerce_coupon_date_evaluate_date']['end_date'] = array(
      'group' => t('Commerce Coupon'),
      'title' => t('Condition: End Date'),
      'help' => t('The end date condition field on a voucher, if any.'),
      'real field' => 'commerce_coupon_conditions_condition_settings',
      'field' => array(
        'settings key' => 'end',
        'handler' => 'blimp_vouchers_handler_field_condition_settings',
        'real field' => 'commerce_coupon_conditions_condition_settings',
        'field_name' => 'condition_end_date',
        'click sortable' => FALSE,
      ),
    );
  }
}

function blimp_vouchers_views_query_alter(&$view, &$query) {
  if ($view->name == 'blimp_vouchers_account_credit' && $view->current_display == 'summary_pane') {
    foreach ($query->tags as $key => $tag) {
      if ($tag == 'commerce_order_access') {
        unset($query->tags[$key]);
      }
    }
    foreach ($query->where as &$condition_group) {
      foreach ($condition_group['conditions'] as &$condition) {
        // Our contextual filter needs to be optional since we want coupons without an order application to show.
        if ($condition['field'] == 'commerce_order_commerce_line_item.uid = :commerce_order_uid ') {
          $condition['field'] = ' (' . $condition['field'] . ' OR commerce_order_commerce_line_item.uid IS NULL) ';
        }
      }
    }
    // Our tables need to join properly based on line item type and order status, otherwise things
    // will be messed up when the normal WHERE clause excludes joined rows on these criteria.
    $query->table_queue['blimp_voucher_commerce_coupon']['join']->extra = array(
      array(
        'field' => 'type',
        'value' => 'blimp_vouchers_discount',
      ),
    );
    $query->table_queue['commerce_order_commerce_line_item']['join']->extra = array(
      array(
        'field' => 'status',
        'value' => 'invoiced',
      ),
    );
    $query->fields['blimp_voucher_commerce_coupon__field_data_commerce_total_com']['table'] = NULL;
    $query->fields['blimp_voucher_commerce_coupon__field_data_commerce_total_com']['field'] = "IF(commerce_order_commerce_line_item.status = 'invoiced', blimp_voucher_commerce_coupon__field_data_commerce_total.commerce_total_amount, 0)";
  }
  if ($view->name == 'blimp_vouchers_records') {
    // Our tables need to join properly based on line item type and order status, otherwise things
    // will be messed up when the normal WHERE clause excludes joined rows on these criteria.
    $query->table_queue['blimp_voucher_commerce_coupon']['join']->extra = array(
      array(
        'field' => 'type',
        'value' => 'blimp_vouchers_discount',
      ),
    );
    $query->table_queue['commerce_order_commerce_line_item']['join']->extra = array(
      array(
        'field' => 'status',
        'value' => 'invoiced',
      ),
    );
  }
}
