<?php

class blimp_vouchers_handler_area_account_summary extends views_handler_area {

  public function render($empty = FALSE) {
    $uid_arg = $this->view->argument['uid'];
    if (!isset($uid_arg->value[0])) {
      return;
    }
    $account = user_load($uid_arg->value[0]);

    $next_order = '';
    $order_wrapper = blimp_vouchers_current_recurring_order($account);
    if ($order_wrapper->getIdentifier()) {
      $data = $order_wrapper->commerce_order_total->data->value();
      foreach ($data['components'] as $component) {
        if ($component['name'] == 'voucher') {
          $next_order = "A credit of <strong>" . commerce_currency_format(-1 * $component['price']['amount'], $component['price']['currency_code']) . "</strong> has been automatically applied to this month's order. These credits will be removed when the order is processed.";
          break;
        }
      }
    }
    $currency = blimp_vouchers_determine_user_currency($account);
    $balance = &drupal_static('blimp_vouchers_account_credit_balance');

    return "<div class='balance'><div class='available'><label>Current credit available:</label> " . commerce_currency_format($balance, $currency) . "</div><div class='message'>" . $next_order . "</div></div>";
  }

}
