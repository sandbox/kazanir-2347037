<?php

class blimp_vouchers_handler_area_add_voucher extends views_handler_area {

  public function render($empty = FALSE) {
    $uid_arg = $this->view->argument['uid'];
    if (!isset($uid_arg->value[0])) {
      return;
    }

    $voucher_form = drupal_get_form('blimp_vouchers_profile_form', $uid_arg->value[0]);
    return drupal_render($voucher_form);
  }

}