<?php

class blimp_vouchers_handler_field_coupon_account_credit extends views_handler_field_custom {

  function render($values) {
    $account = user_load($this->view->args[0]);
    $currency = blimp_vouchers_determine_user_currency($account);
    $fieldname = 'field_' . _blimp_vouchers_price_fields($currency); 
    $field = $values->{$fieldname}[0]['rendered'];
    
    return ($field['#access'] ? $field['#markup'] : '');
  }

}