<?php

/*
 * Implements hook_rules_condition_info().
 */
function blimp_vouchers_rules_condition_info() {
  $inline_conditions = inline_conditions_get_info();
  
  $conditions['blimp_vouchers_maximum_usage'] = array(
    'label' => t('Voucher usage'),
    'description' => t('Enter the maximum number of accounts that may redeem a voucher.'),
    'parameter' => array(
      'commerce_coupon' => array(
        'type' => 'commerce_coupon',
        'label' => t('Coupon'),        
      ),
      'max_usage' => array(
        'type' => 'integer',
        'label' => t('Max usage')
      )
    ),
    'callbacks' => array(
      'execute' => $inline_conditions['blimp_vouchers_maximum_usage']['callbacks']['build'],
    )
  );
  
  return $conditions;
}

function blimp_vouchers_get_usage($coupon_id) {
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'user')
    ->fieldCondition('blimp_voucher', 'target_id', $coupon_id);
  
  return $query
    ->count()
    ->execute();
}

/**
 * Get the maximum allowed uses for a particular voucher.
 * 
 * @param type $coupon_id
 * @return type
 */
function blimp_vouchers_get_max_usage($coupon_id) {
  $coupon = commerce_coupon_load($coupon_id);
  if (!in_array($coupon->type, commerce_coupon_get_types())) {
    return;
  }
  
  $coupon_wrapper = entity_metadata_wrapper('commerce_coupon', $coupon);
  $usage = 0;  
  
  if ($coupon_wrapper->commerce_coupon_conditions->value()) {
    $conditions = $coupon_wrapper->commerce_coupon_conditions->value();
    foreach ($conditions as $condition) {
      // It is possible that there could be more than one usage constraint
      // although there is no real reason for doing this.
      if ($condition['condition_name'] == 'blimp_vouchers_maximum_usage' && !empty($condition['condition_settings']['max_usage'])) {
        if (!$usage || $condition['condition_settings']['max_usage'] < $usage) {
          $usage = $condition['condition_settings']['max_usage'];
        }
      }
    }
  }

  return $usage;
}

/*
 * Implements hook_inline_conditions_info().
 */
function blimp_vouchers_inline_conditions_info() {
  // Usage module only runs its conditions when the user is attempting to redeem
  // a coupon, which we call the "pre-redeem" phase.
  $conditions['blimp_vouchers_maximum_usage'] = array(
    'label' => t('Voucher usage (accounts)'),
    'entity type' => 'commerce_coupon',
    'callbacks' => array(
      'configure' => 'blimp_vouchers_maximum_usage_configure',
      'build' => 'blimp_vouchers_maximum_usage_build',
    ),
  );
  
  return $conditions;
}

function blimp_vouchers_maximum_usage_configure($settings) {
  if (is_string($settings)) {
    $settings = unserialize($settings);
  }
  
  $form['max_usage'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum usage'),
    '#description' => t('Enter the maximum number of accounts that may redeem this voucher code. Please be sure to understand the implications of picking a number other than 1.'),
    '#default_value' => !empty($settings['max_usage']) ? $settings['max_usage'] : '1',
    '#element_validate' => array('element_validate_integer_positive'),
    '#required' => TRUE,
  );

  return $form;
}

function blimp_vouchers_maximum_usage_build($coupon, $max_usage) {
  $usage = blimp_vouchers_get_usage($coupon->coupon_id);
  
  if ($usage >= $max_usage) {
    // Set an error message in this static variable.
    $error = &drupal_static('commerce_coupon_error_' . strtolower($coupon->code));
    $error = t('Sorry, this voucher has already reached its limit of usage.');
  }
  else {
    return TRUE;
  }  
}